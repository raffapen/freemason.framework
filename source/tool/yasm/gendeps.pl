
use strict;
use File::Temp qw/ :POSIX /;
use Scaffolds::System; 

my $nop_flag = 0;
my $d_file;
my $o_file;
my $cmd;
my $dump_flag = 0;

while (@ARGV)
{
	my $a = $ARGV[0];
	if ($a eq '-d')
	{
		shift;
		$d_file = shift;
	}
	elsif ($a eq '-o')
	{
		shift;
		$o_file = shift;
	}
	elsif ($a eq '-dump')
	{
		shift;
		$dump_flag = 1;
	}
	elsif ($a eq '-n')
	{
		shift;
		$nop_flag = 1;
	}
	elsif ($a eq '-h' || $a eq '-help')
	{
		usage();
		exit(1);
	}
	else
	{
		last;
	}
}

shift if $ARGV[0] == '--';

my $cmd = join(' ', @ARGV);
die "invalid command\n" if $cmd =~ "^-";

$d_file = "$o_file.d" if ! $d_file;

if (!$o_file || !$cmd)
{
	usage();
	die "missing arguments\n";
}

if ($nop_flag)
{
	print "$cmd";
	exit(0);
}

my @out = `$cmd 2>nul`;

my %hash = {};
my @files;

foreach (@out)
{
	next if !(/^%line \S+ (.*)/);
	my $f =$1;
	$f =~ s/\\/\//g;
	$f =~ s/\"//g if $f =~ /\w/;
	$f = trim($f);
	next if exists $hash{$f};
	$hash{$f} = 1;
	push @files, $f;
}

# shift @files; # shift source file out of @files

open DFILE, ">$d_file" or die "cannot create $d_file\n";
print DFILE "$o_file:";
foreach my $f (@files)
{
	print DFILE " \\\n\t$f";
}
print DFILE "\n\n";

foreach my $f (@files)
{
	print DFILE "$f:\n\n";
}

close DFILE;
exit 0;

sub trim
{
	my $s = shift;
	$s =~ s/^\s+//;
	$s =~ s/\s+$//;
	return $s;
}

sub usage
{
	print "usage: rcdep.pl -cc compiler -dep res-file -res obj_file -rc rc-file [compiler arguments]\n";
	print "       rcdep.pl [-h] [-n]\n";
}
