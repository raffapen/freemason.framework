
#----------------------------------------------------------------------------------------------
# MSC v15 (VisualStudio 9.0) definitions

#----------------------------------------------------------------------------------------------
# locations

ifeq ($(MSC_15_PATH),)
$(error MSC_15_PATH undefined. Check host definitions.)
endif

MSC_15_BIN_DIR=$(MSC_15_PATH)/bin

MSC_15_LIB_DIRS = \
	$(MSC_15_PSDK_PATH)/lib \
	$(MSC_15_PATH)/lib \
	$(MSC_15_PATH)/atlmfc/lib

CC_SYS_INCLUDE_DIRS += \
	$(MSC_15_PSDK_PATH)/include \
	$(MSC_15_PATH)/include \
	$(MSC_15_PATH)/atlmfc/include

_CC_LIB += $(addprefix -L,$(MSC_15_LIB_DIRS))

#----------------------------------------------------------------------------------------------
# compiler

ifneq ($(MOCKUP),1)
CC=$(MSC_15_BIN_DIR)/cl.exe
else
CC=$(MK)/../bin/$(HOST_OS)/mockup/cl.exe
CC_INHERENT_DEP_GEN=1
endif

CXX=$(CC)

define MSC_15_COMMON_FLAGS
	-nologo
	-c
	-FC
endef

CC_FLAGS += $(flat $(MSC_15_COMMON_FLAGS)) $(flat $(MSC_FLAGS))

define MSC_15_C_FLAGS
	-TC
endef

CC_C_FLAGS += $(flat $(MSC_15_C_FLAGS)) $(flat $(MSC_C_FLAGS))

define MSC_15_CXX_FLAGS
	-EHsc
endef

CC_CXX_FLAGS += $(flat $(MSC_15_CXX_FLAGS)) $(flat $(MSC_CXX_FLAGS))

#----------------------------------------------------------------------------------------------

CC_STD_PP_DEFS += _MSC_VER=1500
#	__STDC__

CC_PP_DEFS += _CRT_SECURE_NO_WARNINGS

ifeq ($(CC_UNICODE),1)
CC_PP_DEFS += UNICODE _UNICODE
else
CC_PP_DEFS += _MBCS 
endif

#----------------------------------------------------------------------------------------------

OBJ_EXT=.obj

CC_OBJ_FLAGS(FILE)=-Fo$(FILE) -Fd$$(basename $(FILE)).pdb

#----------------------------------------------------------------------------------------------
# Debug flags

# -Gm iff -Zi
CC_FLAGS.DEBUG=-RTC1 -Od -Z7

CC_PP_DEFS.OPT += NDEBUG

CC_FLAGS.OPT.size=-O1
CC_FLAGS.OPT.speed=-O2
CC_FLAGS.OPT.default=$(CC_FLAGS.OPT.speed)

CC_LST_FLAGS.on=-FAcs -Fa$$(basename $$@).lst

CC_RTTI_FLAGS=$(CC_RTTI.$(if $(eq $(CC_RTTI),1),on,off))

CC_RTTI_FLAGS.on=-GR
CC_RTTI_FLAGS.off=-GR-

CC_FLAGS.AS_C=-TC
CC_FLAGS.AS_CXX=-TP

CC_FLAGS.WARN.none=-w
CC_FLAGS.WARN.few=-W2
CC_FLAGS.WARN.normal=-W3
CC_FLAGS.WARN.many=-W4
CC_FLAGS.WARN.all=-Wall
CC_FLAGS.WARN.error=-WX

#----------------------------------------------------------------------------------------------
# CRT selection flags

MSC_CRT_MT ?= 1
MSC_CRT_DLL ?= 0

ifneq ($(MSC_CRT_MT),1)
MSC_CRT_FLAG=-ML
else ifneq ($(MSC_CRT_DLL),1)
MSC_CRT_FLAG=-MT
MSC_STD_PP_DEFS += _MT
else
MSC_CRT_FLAG=-MD
MSC_STD_PP_DEFS += _MT _DLL
endif

ifeq ($(DEBUG),1)
MSC_CRT_FLAG:=$(MSC_CRT_FLAG)d
endif

CC_FLAGS += $(MSC_CRT_FLAG)

CC_STD_PP_DEFS += $(MSC_STD_PP_DEFS)

#----------------------------------------------------------------------------------------------
# ATLMFC

MSC_ATL ?= 0
MSC_MFC ?= 0

MSC_MFC_DLL ?= 0
MSC_MFC_UNICODE ?= $(CC_UNICODE)

ATLMFC_D=$(if $(eq $(DEBUG),1),d,)

MSC_MFC_LIBS.static=$(if $(eq $(MSC_MFC_UNICODE),1),u,n)afxcw$(ATLMFC_D).lib
MSC_MFC_PP_DEFS.static=
MSC_MFC_LIBS.dynamic=mfc90$(ATLMFC_D).lib mfcs90$(ATLMFC_D).lib
MSC_MFC_PP_DEFS.dynamic=_AFXDLL

MSC_ATL_LIBS.static=atls$(ATLMFC_D).lib
MSC_ATL_LIBS.dynamic=atl.lib

ifeq ($(MSC_ATL),1)
MSC_ATL_LIB_TYPE=$(if $(eq $(MSC_ATL_DLL),1),dynamic,static)
MSC_ATL_LIBS=$(MSC_ATL_LIBS.$(MSC_ATL_LIB_TYPE))
endif

ifeq ($(MSC_MFC),1)
MSC_MFC_LIB_TYPE=$(if $(eq $(MSC_MFC_DLL),1),dynamic,static)
MSC_MFC_LIBS=$(MSC_MFC_LIBS.$(MSC_MFC_LIB_TYPE))
endif

MSC_15_LIBS += $(MSC_MFC_LIBS) $(MSC_ATL_LIBS)

#----------------------------------------------------------------------------------------------
# linker

LD=$(MSC_15_BIN_DIR)/link.exe

LD_FLAGS += \
	-nologo \
	$(addprefix -libpath:,$(MSC_15_LIB_DIRS)) \
	-map \
	$(if $(CC_STACK_SIZE),-stack:$(CC_STACK_SIZE))

MSC_LD_PDB ?= 1

LD_FLAGS.PDB=$(if $(eq $(MSC_LD_PDB),1),-pdb:$@.pdb,)
LD_FLAGS.DEBUG += -debug $(LD_FLAGS.PDB)

LD_TARGET_FLAGS(TARGET) += -out:$(TARGET)

LD_FILES.SYS += $(addprefix $(MSC_15_PSDK_PATH)/lib/,$(flat $(MSC_15_PSDK_LIBS))) $(flat $(MSC_15_LIBS))

define MSC_15_PSDK_LIBS
	kernel32.lib
	user32.lib
	gdi32.lib
	winspool.lib
	comdlg32.lib
	advapi32.lib
	shell32.lib
	ole32.lib
	oleaut32.lib
	uuid.lib
	mgmtapi.lib
	snmpapi.lib
	ws2_32.lib
	IPHlpApi.lib
	odbc32.lib
	odbccp32.lib
endef

#----------------------------------------------------------------------------------------------
# resource compiler

MSC_RC=$(MSC_15_PSDK_PATH)/bin/rc.exe
MSC_RC_FLAGS=/l 0x40d

MSC_RC_PP_DEFS=RC_INVOKED
CC_PP_FLAGS += $(if $(eq $(DEBUG),1),$(CC_PP_FLAGS.DEBUG))
CC_PP_FLAGS += $(if $(eq $(OPT),1),$(CC_PP_FLAGS.OPT))

MSC_RC_PP_FLAGS=$(MSC_RC_PP_DEFS:%=/d%)
MSC_RC_INCLUDE_FLAGS=$(CC_INCLUDE_FLAGS:-I%=/i%)

define MSC_RES_FILTER(FILES)
	$(patsubst %.rc,%.res,$(filter $(addprefix %,.rc),$(FILES)))
endef

_MSC_RES_FILES(FILES)=$(addprefix $(BIN)/,$(call MSC_RES_FILTER,$(FILES)))

MSC_RES_FILES += $(call _MSC_RES_FILES,$(CC_SRC_FILES))

OBJ_FILES += $(MSC_RES_FILES)

MSC_RC_DEP_FLAGS=$(MSC_RC_PP_FLAGS:/d%=-D%) $(MSC_RC_INCLUDE_FLAGS:/i%=-I%)

define MSC_RC_DEPENDS_CMD(SOURCE,OBJECT,BASE_SOURCE_DIR)
perl $(MK)/tool/msc-15/rcdep.pl -cc $(CC) -dep $(call CC_DEPENDS_FILE,$(SOURCE),$(BASE_SOURCE_DIR)) \
	-res $(OBJECT) -rc $(SOURCE) $(MSC_RC_DEP_FLAGS)
endef

define MSC_RC_CMD(SOURCE,OBJECT,BASE_SRC_DIR)
$(MSC_RC) $(MSC_RC_FLAGS) $(MSC_RC_PP_FLAGS) $(MSC_RC_INCLUDE_FLAGS) /fo$(OBJECT) $(SOURCE) ; \
$(call CC_VERIFY_OBJ_CMD,$(OBJECT))
endef

#----------------------------------------------------------------------------------------------
# kw

CC_KW_CONFIG_CMD=$(CC) -nologo -c -Fo$(tmpfile)
