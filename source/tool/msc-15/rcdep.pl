
use strict;
use File::Temp qw/ :POSIX /;
use Scaffolds::System; 

my $nop_flag = 0;
my $d_file;
my $rc_file;
my $res_file;
my $cc_prog;

while (@ARGV)
{
	my $a = $ARGV[0];
	if ($a eq '-dep')
	{
		shift;
		$d_file = shift;
	}
	elsif ($a eq '-res')
	{
		shift;
		$res_file = shift;
	}
	elsif ($a eq '-rc')
	{
		shift;
		$rc_file = shift;
	}
	elsif ($a eq '-cc')
	{
		shift;
		$cc_prog = shift;
	}
	elsif ($a eq '-n')
	{
		shift;
		$nop_flag = 1;
	}
	elsif ($a eq '-h' || $a eq '-help')
	{
		usage();
		exit(1);
	}
	else
	{
		last;
	}
}

my $opt = join(' ', @ARGV);

if (!$d_file || !$res_file || !$rc_file)
{
	usage();
	die "missing arguments\n";
}

if ($nop_flag)
{
	print "$cc_prog /u /E /DRC_INVOKED $opt $rc_file";
	exit(0);
}

my $cc = new Scaffolds::System("$cc_prog /u /E /DRC_INVOKED $opt $rc_file", { outlog => 0 });
die "error in resource file compilation\n" if $cc->retcode;

my %hash = {};
my @files;

foreach ($cc->out)
{
	next if !(/^#line \d+ (.*)/);
	my $f =$1;
	$f =~ s/\\\\/\//g;
	$f =~ s/\"//g if $f =~ /\w/;
	$f = trim($f);
	next if exists $hash{$f};
	$hash{$f} = 1;
	push @files, $f;
}

shift @files; # shift source file out of @files

open DFILE, ">$d_file" or die "cannot create $d_file\n";
print DFILE "$res_file: $rc_file";
foreach my $f (@files)
{
	print DFILE " \\\n\t$f";
}
print DFILE "\n\n";

foreach my $f (@files)
{
	print DFILE "$f:\n\n";
}

close DFILE;
exit 0;

sub trim
{
	my $s = shift;
	$s =~ s/^\s+//;
	$s =~ s/\s+$//;
	return $s;
}

sub usage
{
	print "usage: rcdep.pl -cc compiler -dep res-file -res obj_file -rc rc-file [compiler arguments]\n";
	print "       rcdep.pl [-h] [-n]\n";
}
