
#----------------------------------------------------------------------------------------------
# GCC 4 definitions

#----------------------------------------------------------------------------------------------
# locations

GCC_4_BIN_DIR=$(GCC_4_PATH)/bin

define GCC_4_SYS_INCLUDE_DIRS
#	the following is due to "error: map: Permission denied"
	$(GCC_4_PATH)/$(GCC_TARGET_ARCH)/include/c++/$(GCC_VER)
endef

CC_SYS_INCLUDE_DIRS += $(flat $(GCC_4_SYS_INCLUDE_DIRS))

GCC_4_SYSROOT_FLAG=--sysroot=$(GCC_4_SYSROOT_PATH)

#----------------------------------------------------------------------------------------------
# compiler

ifneq ($(MOCKUP),1)
CC=$(GCC_4_BIN_DIR)/$(GCC_TARGET_ARCH)-gcc$(HOST_PROG_EXT)
CXX=$(GCC_4_BIN_DIR)/$(GCC_TARGET_ARCH)-g++$(HOST_PROG_EXT)
else
CC=$(MK)/../bin/$(HOST_OS)/mockup/gcc$(HOST_PROG_EXT)
CXX=$(MK)/../bin/$(HOST_OS)/mockup/g++$(HOST_PROG_EXT)
endif

STRIP=$(GCC_4_BIN_DIR)/$(GCC_TARGET_ARCH)-strip$(HOST_PROG_EXT)

#----------------------------------------------------------------------------------------------

define GCC_4_COMMON_FLAGS
	$(GCC_4_SYSROOT_FLAG)
	-c
	-pipe

	-pthread

	-fexceptions
	-fms-extensions

	$(if $(neq $(GCC_NO_64_BIT_SWITCHES),1),
		$(if $(eq $(TARGET_ARCH_32BIT),1),-m32,)
		$(if $(eq $(TARGET_ARCH_64BIT),1),-m64,),)

	$(if $(eq $(CC_PIC),1),-fPIC,)

	$(if $(eq $(CC_ARCH_SOFT_FLOAT),1),-msoft-float,)
	$(if $(eq $(CC_ARCH_SSE4.2),1),-msse4.2,)
endef

CC_COMMON_FLAGS += $(flat $(GCC_4_COMMON_FLAGS)) $(flat $(GCC_FLAGS))

define GCC_4_C_FLAGS
#	-nostdinc
endef

CC_C_FLAGS += $(flat $(GCC_4_C_FLAGS)) $(flat $(GCC_C_FLAGS))

define GCC_4_CXX_FLAGS
	-fpermissive
#	-nostdinc++
endef

CC_CXX_FLAGS += $(flat $(GCC_4_CXX_FLAGS)) $(flat $(GCC_CXX_FLAGS))

#----------------------------------------------------------------------------------------------

CC_FLAGS.DEBUG=-g

CC_FLAGS.OPT.common=-g

CC_FLAGS.OPT.size=-Os
CC_FLAGS.OPT.speed=-O3
CC_FLAGS.OPT.default=$(CC_FLAGS.OPT.speed)

define CC_LST_FLAGS.on
endef

CC_RTTI_FLAGS.on=
CC_RTTI_FLAGS.off=-fno-rtti

CC_FLAGS.AS_C=-x c
CC_FLAGS.AS_CXX=-x c++

#----------------------------------------------------------------------------------------------
# compiler warnings

CC_FLAGS.WARN.none=-w
CC_FLAGS.WARN.all=-Wall
CC_FLAGS.WARN.error=-Werror

define CC_FLAGS.WARN.normal
	-Wreturn-type
	-Wformat
	$(if $(eq $(OPT),1),-Wuninitialized)
	-Wsequence-point
endef

define CC_FLAGS.WARN.more
	-Wswitch
	-Wextra
endef

define CC_FLAGS.CXX_WARN.normal
endef

define CC_FLAGS.CXX_WARN.less
	-Wno-deprecated
	-Wno-reorder
	-Wno-write-strings
endef

define CC_FLAGS.CXX_WARN.more
#	-Wno-conversion-null
endef

#----------------------------------------------------------------------------------------------
# object files

OBJ_EXT=.o

CC_OBJ_FLAGS(FILE)=-o $(FILE)

#----------------------------------------------------------------------------------------------
# preprocessor

define GCC_4_PP_DEFS
endef

CC_PP_DEFS += $(flat $(GCC_4_PP_DEFS))

CC_INHERENT_CPP=1

define CC_CPP_DIAG_C_CMD(SOURCE)
$(CC) -E -dID $(CC_FLAGS) $(CC_PP_FLAGS) $(CC_C_FLAGS) $(CC_INCLUDE_FLAGS) $(SOURCE) > $(call CPP_DIAG_FILE,$(SOURCE))
endef

define CC_CPP_DIAG_CXX_CMD(SOURCE)
$(CXX) -E -dID $(CC_FLAGS) $(CC_PP_FLAGS) $(CC_CXX_FLAGS) $(CC_INCLUDE_FLAGS) $(SOURCE) > $(call CPP_DIAG_FILE,$(SOURCE))
endef

#----------------------------------------------------------------------------------------------
# coverage analysis

CC_COVERAGE_FLAGS.on = --coverage -ftest-coverage -fprofile-arcs
LD_COVERAGE_FLAGS.on = -ftest-coverage -fprofile-arcs

#----------------------------------------------------------------------------------------------
# linker

LD=$(CXX)

define GCC_4_LD_FLAGS
	$(GCC_4_SYSROOT_FLAG)
	-Wl,-Bsymbolic
	-rdynamic
	-g

	$(if $(eq $(TARGET_ARCH_32BIT),1),-m32,)
	$(if $(eq $(TARGET_ARCH_64BIT),1),-m64,)

#	$(if $(eq $(CC_SOFT_FLOAT),1),-msoft-float,)
endef

define GCC_4_LD_STDLIBS
	-lstdc++
	-lm
	-lpthread
	-lrt
	-lncurses
	-lcrypt

#	$(if $(eq $(GCC_LD_USE_DL),1),-ldl,)
endef

LD_FLAGS += $(flat $(GCC_4_LD_FLAGS)) $(LD_MAP_FLAGS) $(LD_DIRS_FLAGS) $(GCC_LD_FLAGS)
LD_STDLIBS = $(flat $(GCC_4_LD_STDLIBS))

LD_MAP_FLAGS=-Wl,-M,-Map,$(TARGET_FILE).map,--cref

LD_DIRS_FLAGS=$(LD_LIB_DIRS:%=-L%)

LD_TARGET_FLAGS(TARGET) += -o $(TARGET)

LD_FILES.SYS += 

STRIPPED_TARGET=$(_TARGET).stripped

CLEAN_FILES += $(_TARGET).map $(STRIPPED_TARGET)

#----------------------------------------------------------------------------------------------

CC_INHERENT_DEP_GEN=1

CC_DEPENDS_FLAGS(FILE,BASE_DIR)=-MD -MP -MF $(call CC_DEPENDS_FILE,$(FILE),$(BASE_DIR)) 

#----------------------------------------------------------------------------------------------
# kw

CC_KW_CONFIG_CMD=$(CC) -c -o $(tmpfile)
CC_KW_EXTRA_FLAGS=-I$(GCC_4_SYSROOT_PATH)
