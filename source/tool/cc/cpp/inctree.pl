
use strict;

my $ifile = $ARGV[0];
open IFILE, "<$ifile";
my @stack;
my $lastline;
my $line;
my $globalline = 0;
foreach (<IFILE>)
{
	++$globalline;
	my $globalline_pref = "$globalline" . " " x (8 - length("$globalline"));

	next if ! ($_ =~ "# ([0-9]*) \"(.*)\"");
	my $node = $2;
	$line = $1;
	next if $node eq '<built-in>';
	next if $node eq '<command line>';
	if ($#stack == -1)
	{
		push @stack, $node;
		$lastline = $line;
		print $globalline_pref . "$node\n";
	}
	else
	{
		my $lasti = $#stack;
		my $last = $stack[$lasti];

		next if $last eq $node;
	       	if ($lasti > 0)
		{
			my $parent = $stack[$lasti - 1];
			if ($parent eq $node)
			{
				pop @stack;
				next;
			}
		}
		push @stack, $node;
		print $globalline_pref . "\t"x($lasti+1) . "$node at $lastline\n";
	}
}
continue
{
	$lastline = $line;
}
close IFILE;
