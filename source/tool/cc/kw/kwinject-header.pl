
use File::Spec;
use File::Temp qw/tempfile tempdir/;

$root = $ENV{FREEMASON_ROOT};
$root =~ s/\\/\//g;

$dir = File::Spec->tmpdir;

($fh, $c_file) = tempfile("kwXXXX", SUFFIX => '.c', UNLINK => 1, DIR => $dir);
print $fh "int main() { return 0; }\n";
close $fh;

($fh, $kw_file) = tempfile("kwXXXX", UNLINK => 1, DIR => $dir);
close $fh;

# ($fh, $o_file) = tempfile("kwXXXX", SUFFIX => ".o", UNLINK => 1, DIR => $dir);
# close $fh;

$cc = join(' ', @ARGV);
# $kwinject = $ENV{KLOCWORK_8_0_BIN_PATH} . "/kwinject";
$kwinject = "$root/tools/klocwork/8.0/bin/kwinject";
$cfg = "$root/tools/klocwork/8.0/config/kwfilter.conf";
system("$kwinject -c $cfg -o $kw_file $cc $c_file > nul") and die;

open F, "<$kw_file" or die;
while (<F>)
{
	next if /^compile;/;
	$_ =~ s/\\/\//g;
	print $_;
}

close F;
exit 0;
