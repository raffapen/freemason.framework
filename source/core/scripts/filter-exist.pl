
use strict;

my @lists;
my @files;

while (@ARGV)
{
	my $a = $ARGV[0];
	if ($a eq '-f')
	{
		shift;
		push @lists, shift;
	}
	elsif ($a eq '-h' || $a eq '-help')
	{
		usage();
		exit(1);
	}
	else
	{
		push @files, shift;
	}
}

foreach my $list (@lists)
{
	open F, "<$list" or die;
	while (<F>)
	{
		foreach (split(/\s+/, $_))
		{
			my $s = $_;
			$s =~ s/^\s+|\s+$//g;
			next if $s eq ""; 
			push @files, $s;
		}
	}
	close F; 
}

foreach my $f (@files)
{
	print "$f " if ! -e $f;
}

exit(0);

sub usage
{
	print "usage: filter-exist.pl -f file names...\n";
	print "       filter-exist.pl [-h]\n";
} 	
