use strict;

my $in = shift;
my $out = shift;

open IN, "<$in" or die;
if ($out)
{
	open OUT, ">$out" or die;
}

while (<IN>)
{
	chomp;
	my @files = split(' ', $_);
	foreach (@files)
	{
		open F, "<$_" or die;
		while (<F>)
		{
			chomp;
			print "$_\n" if ! $out;
			print OUT "$_\n" if $out;
		}
		close F;
	}
}
close IN;
close OUT if $out;

exit 0;
