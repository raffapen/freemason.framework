
use strict;
use File::Basename;
use File::Copy;
use File::Path;
use File::stat;
use Time::localtime;

use Scaffolds::System;
use Scaffolds::Util;

my $files_list;
my @files;
my $src_dir = ".";
my $dest_dir;
my $verbose = 0;

while (@ARGV)
{
	my $a = $ARGV[0];
	if ($a eq '--files')
	{
		shift;
		$files_list = shift;
	}
	elsif ($a eq '--from')
	{
		shift;
		$src_dir = shift;
		die "invalid source directory.\n" if $src_dir =~ /^--/;
	}
	elsif ($a eq '--to')
	{
		shift;
		$dest_dir = shift;
		die "invalid source directory.\n" if $dest_dir =~ /^--/;
	}
	elsif ($a eq '--verbose')
	{
		shift;
		$verbose = 1;
	}
	elsif ($a eq '--')
	{
		shift;
		last;
	}
	elsif ($a =~ /^-/)
	{
		die "$a: invalid argument\n";
	}
	else
	{
		last;
	}
}

@files = map { split(/\s+/) } read_list($files_list) if $files_list;

push @files, @ARGV;

foreach my $file (@files)
{
	my $src = "$src_dir/$file";
	my $dest = "$dest_dir/$file";
	
	die "$src does not exist\n" if ! -e $src;
	
	if (! -e $dest)
	{
		print "$dest does not exist. Copying $src.\n" if $verbose;
		
		my $d = dirname($dest);
		if (! -e $d)
		{
			print "creating directory $d.\n" if $verbose;
			mkpath($d) or die "cannot create directory $d\n";
		}
		print "copy $src to $dest\n" if $verbose;
		copy($src, $dest) or die "cannot copy $src to $dest\n";

		# add file to ClearCASE
		print "adding $dest to ClearCASE\n" if $verbose;
		my $add = new Scaffolds::System("ct1 add $dest");
		die "adding $dest to ClearCASE failed.\n" if $add->retcode;
	}
	else
	{
		# check if source is newer than destination
		my $src_time = file_time($src);
		my $dest_time = file_time($dest);
		if ($src_time > $dest_time)
		{
			# if file is writible, it is alredy checked-out
			if (! -w $dest)
			{
				print "checking-out $dest.\n" if $verbose;	
				# check-out file
				my $co = new Scaffolds::System("cleartool co -nc -ptime $dest");
				die "checking-out $dest failed.\n" if $co->retcode;
			}
			else
			{
				print "$dest is checked-out.\n" if $verbose;	
			}

			print "copy $src to $dest\n" if $verbose;
			copy($src, $dest) or die "cannot copy $src to $dest\n";
		}
		else
		{
			print "$src is newer than $dest: no save needed\n" if $verbose;
		}
 	}
}

exit 0;

sub file_time
{
	my ($file) = @_;
	my $time = stat($file)->mtime;
	return $time;
}
